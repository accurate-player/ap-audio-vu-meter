import React, { Component } from "react";
import "./App.css";
import { getMediaInfo } from "@accurate-player/probe";
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";
import {
  AudioScrubPlugin,
  ChannelControlPlugin,
  HotkeyPlugin,
  SmoothTimeUpdatePlugin,
  DiscreteAudioPlugin,
  DiscreteAudioEventType
} from "@accurate-player/accurate-player-plugins";
import "@accurate-player/accurate-player-controls";
import { ApAudioMeterBasic } from "@accurate-player/accurate-player-components-external";
import "@accurate-player/accurate-player-components-external/dist/styles/theme-dark.css"

ApAudioMeterBasic.register();

const AUDIO_TRACKS = [
  {
    track: {
      label: "Without voices",
      src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/aac-discrete/sintel-no-speak.mp4",
      channelCount: 2,
    },
    enabled: false,
  },
  {
    track: {
      label: "Stereo track",
      src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/aac-discrete/sintel_stereo.mp4",
      channelCount: 2,
    },
    enabled: false,
  },
  {
    track: {
      label: "Tears of Steel 5.1",
      src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/aac-discrete/TOS-Surround.mp4",
      channelCount: 6,
      offset: 0,
    },
    enabled: false,
  },
  {
    track: {
      label: "Tears of Steel stereo",
      src: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/aac-discrete/TOS-Stereo.mp4",
      channelCount: 2,
      offset: 0,
    },
    enabled: false,
  },
];

class App extends Component {
  videoElement;
  apControls;
  player;
  discreteAudioPlugin;
  discreteAudioMeters = [];

  constructor(props) {
    super(props);
    this.state = {
      videoLoaded: false,
      audioTracks: AUDIO_TRACKS
    }
  }

  componentDidMount() {
    this.player = new ProgressivePlayer(this.videoElement, window.LICENSE_KEY);
    new SmoothTimeUpdatePlugin(this.player);
    new ChannelControlPlugin(this.player);
    new HotkeyPlugin(this.player);
    new AudioScrubPlugin(this.player);
    this.discreteAudioPlugin = new DiscreteAudioPlugin(this.player);

    getMediaInfo("https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/timecode/sintel-2048-timecode-stereo.mp4",
      { label: "Sintel Timecode", dropFrame: false }).then(file => {
      this.player.api.loadVideoFile(file);

      this.setState({ videoLoaded: true })

      this.initAudioMeters();
      this.initControls();

      // Load discrete audio tracks
      this.discreteAudioPlugin.initDiscreteAudioTracks(AUDIO_TRACKS);
    });

    // Listen for event - new discrete audio added
    this.discreteAudioPlugin.addEventListener(DiscreteAudioEventType.Added, (event) => {
      console.log('Added audio', event);
      this.setDiscreteAudioMeter(event.target.src, true);
    });

    // Listen for event - new discrete audio removed
    this.discreteAudioPlugin.addEventListener(DiscreteAudioEventType.Removed, (event) => {
      console.log('Removed audio', event);
      this.setDiscreteAudioMeter(event.target.src, false);
    });
  }

  // Initializes all audio meters
  initAudioMeters() {
    const audioMeters = document.querySelectorAll("ap-audio-meter-basic");

    audioMeters.forEach(audioMeter => {
      audioMeter.player = this.player;
    });
  }

  // Returns the offset of the audio track
  audioTrackOffset(src) {
    if (!src) {
      return -1;
    }

    for (let i = 0; i < AUDIO_TRACKS.length; i++) {
      const audio = AUDIO_TRACKS[i];
      if (audio.track.src === src) {
        return i;
      }
    }

    return -1;
  }

  // Enabled or disables a discrete audio meter
  setDiscreteAudioMeter(src, enabled) {
    console.log('Enabling discrete audio meter', src);

    const offset = this.audioTrackOffset(src);

    if (offset === -1) {
      console.warn('No audio track found matching src');
      return;
    }

    AUDIO_TRACKS[offset].enabled = enabled;

    this.setState({
      audioTracks: AUDIO_TRACKS
    });
  }

  initControls() {
    // Check if method exist, otherwise wait 100ms and try again.
    if (!this.apControls.init) {
      setTimeout(() => {
        this.initControls();
      }, 100);
      return;
    }

    this.apControls.init(this.videoElement, this.player, {
      saveLocalSettings: true
    });
  }

  render() {
    return (
      <div className="App">
        <div className="logo"></div>
        <div className="video-container">
          <video
            ref={ref => (this.videoElement = ref)}
            crossOrigin="true"
            playsInline={true}/>
          <apc-controls
            ref={ref => (this.apControls = ref)}/>
        </div>

        <div className="audio-meters">
          {this.state.videoLoaded && <ap-audio-meter-basic channelWidth="20"></ap-audio-meter-basic>}

          {this.state.videoLoaded && this.state.audioTracks.map((element, index) =>
            <ap-audio-meter-basic
              style={{display: element.enabled ? 'flex' : 'none' }}
              ref={ref => (this.discreteAudioMeters[index] = ref)}
              key={index}
              source={element.track.src}
              channelWidth="20">
            </ap-audio-meter-basic>
          )}
        </div>
      </div>
    );
  }
}

export default App;
