# Audio VU Meter

This example shows how to integrate the audio VU meter in the AP component library together with the player.

## Demo

![Audio VU meter example](app.png)

## Setup

To setup the demos execute the following:

1. Login to Accurate Player npm repository to gain access to required dependencies, username and password will be provided to you by the Accurate Player team.
> npm login adduser --registry=https://codemill.jfrog.io/codemill/api/npm/accurate-video/ --scope=@accurate-player

2. Go to `shared/license-key-example.js` and follow the instructions of how to provide your license key correctly.

## Running the demo

1. `npm install`
2. `npm run start`
3. Open `http://localhost:3000` in your browser.
